package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;


public class Controller {

    @FXML
    public TextField A;
    @FXML
    public TextField B;
    @FXML
    public TextField C;
    @FXML
    public Button btnDecide;
    @FXML
    public TextField txtX1;
    @FXML
    public TextField txtX2;

    double a = 0;
    double b = 0;
    double c = 0;
    double discreminant = 0;


    public void introduceA(ActionEvent actionEvent) {
        a = Double.parseDouble(A.getText());
    }

    public void introduceB(ActionEvent actionEvent) {
        b = Double.parseDouble(B.getText());
    }

    public void introduceC(ActionEvent actionEvent) {
        c = Double.parseDouble(C.getText());
    }

    public void decide(ActionEvent actionEvent) {
        discreminant = b*b - (4*a*c);

    }

    public void answerX1(ActionEvent actionEvent) {
        if (discreminant < 0){
            txtX1.setText("Нет корней");
        }
        else if(discreminant > 0){
            double x1 = (-(b) + Math.sqrt(discreminant)) / (2 * a);
            txtX1.setText(x1);
        }
        else (discreminant == 0){
            double x = (-b) / (2*a);
            txtX1.setText(x);
        }
    }

    public void answerX2(ActionEvent actionEvent) {
        if (discreminant < 0) {
            txtX1.setText("Нет корней");
        }
        else if(discreminant > 0){
            double x2 = (-(b) - Math.sqrt(discreminant)) / (2 * a);
            txtX1.setText(x2);
            }
            else (discreminant == 0){
                double x = (-b) / (2*a);
                txtX1.setText(x);
            }
    }
}

