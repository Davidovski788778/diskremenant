package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Вычисляем корни квадратного уравнения ax2+bx+c=0. Братан введи a,b,c и нажми Решить и списывай!!!!");
        primaryStage.setScene(new Scene(root, 750, 300));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
